<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * GROUP ROUTES
 */
Auth::routes();


/**
 * GLOBAL ROUTES
 */
Route::get('/', function () {
    return view('welcome');
});
Route::get('/restaurante', function() {
    return view('restaurante.restaurante');
});

/**
 * AUTHENTICATED ROUTES
 */
Route::group(['middleware' => 'auth'], function () {
});

/**
 * TESTING ROUTES
 */
Route::get('/mail', 'TestController@mail');
