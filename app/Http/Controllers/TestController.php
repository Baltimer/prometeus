<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    public function mail(){
        Mail::to('lluiscf92@gmail.com')->send(new \App\Mail\Welcome\Welcome);
    }
}
