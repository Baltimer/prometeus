<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create(
            [
                'name' => 'Lluis',
                'email' => 'lluiscf92@gmail.com',
                'password' => bcrypt('password'), // secret
                'remember_token' => str_random(10),
            ]
        );
        factory(App\User::class, 10)->create();
    }
}
