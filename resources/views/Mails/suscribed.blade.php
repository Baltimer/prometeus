@component('mail::message')
# BIENVENIDO

Te acabas de suscribir a Prometeus

@component('mail::button', ['url' => url('/')])
Página princial
@endcomponent

Muchas gracias,<br>
{{ config('app.name') }}

@endcomponent