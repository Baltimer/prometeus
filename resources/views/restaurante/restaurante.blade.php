@extends('displays.master')

@yield('Inicio - Prometeus')

@section('importCSS')
    <link type="text/css" rel="stylesheet" href="{{ asset('css/desktop/restaurante/restaurante.css') }}"></link>
@endsection

@section('content')
<main>
    <div class="row no-margin">
        <div class="parallax-container">
            <div class="parallax principal-img">
                <img src="img/restaurante.jpg">
            </div>
            <div class="row no-margin title">
                <span id="nombre">Can Pedro</span> <br>
                <span id="subtitulo">El asador número 1</span>
            </div>
        </div>
    </div>
    <div class="row principal no-margin">
        <div class="col s12">
            <div class="row photo no-margin">
                <div class="col s12">
                    <div class="carousel">
                        <a class="carousel-item materialboxed" href="#one!"><img src="img/restaurante.jpg"></a>
                        <a class="carousel-item" href="#two!"><img src="img/restaurante.jpg"></a>
                        <a class="carousel-item" href="#three!"><img src="img/restaurante.jpg"></a>
                        <a class="carousel-item" href="#four!"><img src="img/restaurante.jpg"></a>
                        <a class="carousel-item" href="#five!"><img src="img/restaurante.jpg"></a>
                    </div>
                </div>
            </div>
            <div class="row no-margin">
                <div class="col s6 offset-s1 content" style="background-color: red">
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                </div>
                <div class="col s3 sidecontent" style="background-color: blue">
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                    <p>contenidoodsaokdsaj</p>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('importJS')
    <script>
        $(document).ready(function(){
            $('.parallax').parallax();
            $('.carousel').carousel({
                fullWidth: true,
                dist: 0,
                shift: 0,
                padding: 20
            });
        });
    </script>
@endsection