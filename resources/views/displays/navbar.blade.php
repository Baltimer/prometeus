@php
use Illuminate\Support\Facades\Auth;
@endphp
<header>
    <nav>
        <div class="nav-wrapper">
            <div class="row nav-container">
                <div class="col s2 offset-s1">
                    <a href="{{url('/')}}" class="brand-logo">Prometeus</a>
                </diV>
                <div class="col s6 offset-s2">
                    <ul class="right hide-on-med-and-down">
                        <li class="active"><a href="{{url('/')}}">Inicio</a></li>
                        <li><a href="#">Reservas</a></li>
                        @if(Auth::check())
                            <li><a href="{{route('logout')}}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Logout</a></li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                            </form>
                            <li><a href="#">{{Auth::user()->name}}</a></li>
                        @else
                            <li><a href="{{url('/login')}}">Login</a></li>
                            <li><a href="{{url('/register')}}">Regístrate</a></li>
                        @endif
                    </ul>
                </div>
            </div>
            <ul class="side-nav" id="mobile-demo">
            <li class="active"><a href="{{url('/')}}">Inicio</a></li>
            <li><a href="">Reservas</a></li>
            <li><a href="">Login</a></li>
            <li><a href ="">Registrate</a></li>
            </ul>
        </div>
    </nav>
</header>