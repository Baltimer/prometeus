<footer class="page-footer">
    <div class="row">
        <div class="col l5 offset-l1 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Enlaces</h5>
            <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Política de privacidad</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Aviso legal</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Ley de cookies</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Contáctanos</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="row footer-container">
            <div class="col s5 offset-s1">
                <span>© 2018 Din Gate, SL</span>
            </div>
            <div class="col s3">
                <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
        </div>
    </div>
</footer>