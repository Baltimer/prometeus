<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Prometeus')</title>

    {{--  ASSETS  --}}
    {{--  <style src="{{ asset('css/reset.css') }}"></style>  --}}
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"></link>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/global/navbar.css') }}"></link>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/global/footer.css') }}"></link>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/global/master.css') }}"></link>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    {{--  FIN ASSETS  --}}
    @yield('importCSS')
</head>
<body>
    <div class="main-content">
        @include('displays.navbar')
        @yield('content')
        @include('displays.footer')
    </div>

    <a href="#" class="btn btn-floating red lighten-2 button-collapse menu-button" data-activates="mobile-demo"   ><i class="material-icons">menu</i></a>
    
    {{--  COOKIES SECTION  --}}
    <div class="cookies">
        <div class="row">
            <div class="col s9 offset-s1">
                <p> Usamos cookies para personalizar su experiencia. 
                    Si sigue navegando estará aceptando su uso.
                    <a href="#!"> Más información</a>
                    <span class="close-cookies">X</span>
                </p>
            </div>
        </div>
    </div>
    {{--  END COOKIES SECTION  --}}

    {{--  SCRIPTS ZONE  --}}
    <script>
        $(".button-collapse").sideNav();

        $('.close-cookies').click(function(){
            $('.cookies').hide();
        });
    </script>
    @yield('importJS')
    {{--  END SCRIPTS ZONE  --}}
</body>
</html>