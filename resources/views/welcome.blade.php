@extends('displays.master')

@yield('Inicio - Prometeus')

@section('importCSS')
    <link type="text/css" rel="stylesheet" href="{{ asset('css/desktop/welcome.css') }}"></link>
@endsection

@section('content')
<main>
    <div class="row no-margin">
        <div class="parallax-container">
            <div class="parallax principal-img">
                <img src="{{asset('img/restaurante.jpg')}}">
            </div>
            <div class="row search-form">
                <input class="col s4 offset-s1 search-input autocomplete" autocomplete="off" type="text" id="nombre" name="nombre" placeholder="Nombre del restaurante.."/>
                <input class="col s3 search-input" type="text" id="fecha" name="fecha" placeholder="Selecciona la fecha..   "/>
                <button type="submit" class="waves-effect waves-light btn-large search-button">Buscar</button>
            </div>
        </div>
    </div>
    <div class="row principal no-margin">
        <div class="col s12 m10 offset-m1">
            {{--  NEAR RESTAURANTS ZONE  --}}
            <div class="row">
                <div class="col s12">
                    <h4>Los restaurantes más cercanos</h4>
                    <p>Algunos de los restaurantes más cercanos según tu posición, o la zona que hayas escogido en tu perfil.</p>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <a href="{{url('/restaurante')}}">
                            <div class="card-image">
                                <img src="img/restaurante.jpg">
                            </div>
                            <div class="card-content">
                                <div class="row info-restaurante">
                                    <div class="col s9">
                                        <div class="titulo truncate"><b>Can Pedro</b></div>
                                        <div class=""><i>Carnes y asados</i></div>
                                    </div>
                                    <div class="col s3">
                                        <div class="precio">35€</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="img/restaurante.jpg">
                        </div>
                        <div class="card-content">
                            <div class="row info-restaurante">
                                <div class="col s9">
                                    <div class="titulo truncate"><b>Don Caracol</b></div>
                                    <div class=""><i>Carnes y asados</i></div>
                                </div>
                                <div class="col s3">
                                    <div class="precio">12€</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="img/restaurante.jpg">
                        </div>
                        <div class="card-content">
                            <div class="row info-restaurante">
                                <div class="col s9">
                                    <div class="titulo truncate"><b>Rodeo Grill</b></div>
                                    <div class=""><i>Carnes y asados</i></div>
                                </div>
                                <div class="col s3">
                                    <div class="precio">25€</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="img/restaurante.jpg">
                        </div>
                        <div class="card-content">
                            <div class="row info-restaurante">
                                <div class="col s9">
                                    <div class="titulo truncate"><b>Botavara</b></div>
                                    <div class=""><i>Carnes y mariscada</i></div>
                                </div>
                                <div class="col s3">
                                    <div class="precio">40€</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="img/restaurante.jpg">
                        </div>
                        <div class="card-content">
                            <div class="row info-restaurante">
                                <div class="col s9">
                                    <div class="titulo truncate"><b>El Artista</b></div>
                                    <div class=""><i>Pizza y pastas</i></div>
                                </div>
                                <div class="col s3">
                                    <div class="precio">15€</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="img/restaurante.jpg">
                        </div>
                        <div class="card-content">
                            <div class="row info-restaurante">
                                <div class="col s9">
                                    <div class="titulo truncate"><b>Casa Jacinto</b></div>
                                    <div class=""><i>Carnes y asados</i></div>
                                </div>
                                <div class="col s3">
                                    <div class="precio">35€</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--  END NEAR RESTAURANTS ZONE  --}}

            {{--  RESTAURANTS BY TYPE  --}}
            <div class="row">
                <div class="col s12">
                    <h4>Los restaurantes más variopintos</h4>
                    <p>Selecciona lo que más te apetezca comer y te mostraremos una selección de los mejores restaurantes para que te lo preparen.</p>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m10 offset-m1">
                    <div class="slider">
                        <ul class="slides">
                            <li>
                                <img src="https://static.vix.com/es/sites/default/files/styles/large/public/s/pasta-2.jpg?itok=E5Of6e6Y"> <!-- random image -->
                                <div class="caption center-align">
                                    <h3><span class="tipo-titulo">PASTA</span></h3>
                                    <h5 class="light grey-text text-lighten-3"><span class="tipo-titulo">La pasta más fresca</span></h5>
                                </div>
                            </li>
                            <li>
                                <img src="https://canales.okdiario.com/recetas/img/2017/06/27/entrecot-de-buey-a-la-plancha-655x368.jpg"> <!-- random image -->
                                <div class="caption left-align">
                                    <h3><span class="tipo-titulo">CARNE</span></h3>
                                    <h5 class="light grey-text text-lighten-3"><span class="tipo-titulo">Las mejores carnes y asados</span></h5>
                                </div>
                            </li>
                            <li>
                                <img src="https://clubsusazon.com/wp-content/uploads/sites/2/2016/08/Alambre-800x600.jpg"> <!-- random image -->
                                <div class="caption right-align">
                                    <h3><span class="tipo-titulo">VEGETARIANO</span></h3>
                                    <h5 class="light grey-text text-lighten-3"><span class="tipo-titulo">Las comidas más naturales y frescas<span></h5>
                                </div>
                            </li>
                            <li>
                                <img src="https://i0.wp.com/recetas.paraguay.com/wp-content/uploads/2013/12/plato-de-pescado.jpg?fit=1024%2C682&ssl=1"> <!-- random image -->
                                <div class="caption center-align">
                                    <h3><span class="tipo-titulo">PESCADO</span></h3>
                                    <h5 class="light grey-text text-lighten-3"><span class="tipo-titulo">Lo mejor del mar</span></h5>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {{--  END RESTAURANTS BY TYPE  --}}
        </div>
    </div>
</main>
@endsection

@section('importJS')
    <script>
        $(document).ready(function(){
            $('.parallax').parallax();

            // AUTOCOMPLETE
            $('input.autocomplete').autocomplete({
                data: {
                "Casa Jacinto": null,
                "Rodeo Grill": null,
                "Botavara": null,
                "Don Caracol": null,
                "El Artista": null,
                "Can Pedro": null
                },
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                onAutocomplete: function(val) {
                // Callback function when value is autcompleted.
                },
                minLength: 2, // The minimum length of the input for the autocomplete to start. Default: 1.
            });

            // SLIDER
            $('.slider').slider({
                height: 500
            });

            // FIXED SEARCH-FORM
            var offset = $('.search-form').offset();
            $(window).scroll(function(){
                var scrollTop = $(window).scrollTop();

                if(offset.top < scrollTop){
                    $('.search-form').addClass('fixed');
                    $('.search-input').addClass('fixed-content');
                    $('.search-button').addClass('fixed-content');
                } else {
                    $('.search-form').removeClass('fixed');
                    $('.search-input').removeClass('fixed-content');
                    $('.search-button').removeClass('fixed-content');
                }
            });
        });

        // DATEPICKER
        $('#fecha').pickadate({
            labelMonthNext: 'Mes siguiente',
            labelMonthPrev: 'Mes anterior',
            labelMonthSelect: 'Selecciona un mes',
            labelYearSelect: 'Selecciona un año',
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            weekdaysShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
            monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
            weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
            weekdaysLetter: [ 'D', 'L', 'M', 'X', 'J', 'V', 'S' ],
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 3, // Creates a dropdown of 15 years to control year,
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Ok',
            closeOnSelect: true, // Close upon selecting a date,
            firstDay: 'Lun'
          });

        
    </script>   
@endsection